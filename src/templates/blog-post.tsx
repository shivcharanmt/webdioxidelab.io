import { DiscussionEmbed } from 'disqus-react';
import { graphql } from 'gatsby';
import React from 'react';
import Helmet from 'react-helmet';

import { Layout } from '../components/Layout';

interface IProps {
  data: {
    markdownRemark: {
      id: string;
      html: string;
      frontmatter: {
        title: string;
        date: string;
        keywords: string;
        author: string;
      }
      timeToRead: string;
      tableOfContents: string;
      headings: {
        value: string;
        depth: string;
      }
      excerpt: string;
    }
  }
}

const BlogPost = ({ data }: IProps) => {
  const post = data.markdownRemark;
  const disqusShortname = 'webdioxide';
  const disqusConfig = {
    identifier: post.id,
    title: post.frontmatter.title,
  };
  return (
    <Layout>
      <main className="post-page-wrapper">
        <Helmet
          meta={[
            { name: 'description', content: `${post.frontmatter.date} ${post.excerpt} Read more` },
            { name: 'keywords', content: post.frontmatter.keywords },
            { name: 'author', content: post.frontmatter.author },
          ]}
          script={[
            { src: 'https://static.codepen.io/assets/embed/ei.js' },
          ]}
          title={post.frontmatter.title}
        />
        <section className="post-wrapper">
          <div className="post-content">
            <h1 className="post-title">{post.frontmatter.title}</h1>
            {post.tableOfContents
              && (
                <div
                  className="post-toc mobile-only"
                  dangerouslySetInnerHTML={{ __html: data.markdownRemark.tableOfContents }}
                />
              )}
            <div dangerouslySetInnerHTML={{ __html: post.html }} />
            <DiscussionEmbed config={disqusConfig} shortname={disqusShortname} />
          </div>
        </section>
        <section className="sidebar table-of-contents">
          {post.tableOfContents
            && <h3 className="sidebar-heading"> Table Of Contents </h3>}
          <div dangerouslySetInnerHTML={{ __html: post.tableOfContents }} />
        </section>
      </main>
    </Layout>
  );
};

export default BlogPost;

export const query = graphql`
  query BlogPostQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        date
        keywords
        author
      }
      timeToRead
      tableOfContents
      headings {
        value
        depth
      }
      excerpt
    }
  }
`;
