import { graphql } from 'gatsby';
import React from 'react';
import { Helmet } from 'react-helmet';

import AllPosts from '../components/AllPosts';
import FeaturedPost from '../components/FeaturedPost';
import { Layout } from '../components/Layout';

interface IProps {
  data: {
    allMarkdownRemark: {
      totalCount: number;
      edges: {
        node: {
          id: string;
          frontmatter: {
            title: string;
            date: string;
            image: string;
          };
          fields: {
            slug: string;
          };
          excerpt: string;
        };
      }[];
    };
  };
}

const IndexPage = ({ data }: IProps) => (
  <Layout>
    <div className="wrapper">
      <Helmet
        link={[
          {
            rel: 'icon', type: 'image/png', sizes: '64x64', href: '/publicImages/favicon.png',
          },
        ]}
        meta={[
          { name: 'description', content: 'Webdioxide-The Blog Source' },
        ]}
        title="Webdioxide"
      />
      <FeaturedPost
        postData={data.allMarkdownRemark.edges[0]}
      />
      <AllPosts
        posts={data.allMarkdownRemark.edges.slice(1)}
        featured
      />
    </div>
  </Layout>
);

export default IndexPage;

export const query = graphql`
  query IndexPageQuery {
    allMarkdownRemark(sort: {fields: [frontmatter___date], order: DESC}) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
            image
          }
          fields {
            slug
          }
          excerpt
        }
      }
    }
  }
`;
