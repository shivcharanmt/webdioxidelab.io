import Typography from 'typography';
import grandViewTheme from 'typography-theme-grand-view';
import lincolnTheme from 'typography-theme-lincoln';

lincolnTheme.googleFonts.push({
  name: 'Crimson Text',
  styles: ['400', '400i', '600', '600i', '700', '700i'],
});

lincolnTheme.bodyFontFamily = ['Crimson Text'];

const typography = new Typography(grandViewTheme);

export default typography;
