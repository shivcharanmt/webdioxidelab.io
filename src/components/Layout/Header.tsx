import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import Logo from '../Logo';

interface HeaderProps {
  title: string;
}

export const Header = ({ title }: HeaderProps) => (
  <>
    <title>{title}</title>
    <h1 style={{ margin: 0 }}>
      <Link
        to="/"
      >
        <Logo />
      </Link>
    </h1>
  </>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
};
