import React from 'react';

import Button from './Button';

interface IProps {
  postData: {
    node: {
      frontmatter: {
        image: string;
        title: string;
      };
      excerpt: string;
      fields: {
        slug: string;
      };
    }
  };
}

const FeaturedPost = (props: IProps) => {
  const { postData: { node: { frontmatter: { image, title }, excerpt, fields: { slug } }}} = props;
  return (
    <div
      className="featured-post"
      style={{ backgroundImage: `url(${image})` }}
    >
      <div className="featured-post-content">
        <h1 className="featured-post-title"> {title} </h1>
        <p className="featured-post-excerpt"> {excerpt} </p>
        <div className="featured-btns">
          <Button
            postLink={slug}
          />
        </div>
      </div>
    </div>
  );
};
export default FeaturedPost;
